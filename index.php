<?php
#=================== setting ====================#
use AmoCRM\OAuth2\Client\Provider\AmoCRM;

include_once __DIR__ . '/vendor/autoload.php';
include_once __DIR__ . '/src/AmoCRM.php';

define('HOST', 'https://potapenkov00.amocrm.ru/sapi/v2/leads');
define('TOKEN_FILE', DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'token_info.json');
#================================================#
### run
$provider = new AmoCRM([
    'clientId' => '57167147-b48b-4934-9a05-4e4bc3605d00',
    'clientSecret' => 'abXrLNiQFSWt4B1B0PQxOJ0gQxpbZBQFR6ABHtLjK8S3G9vNyIjJzCa7SrsCOK6b',
    'redirectUri' => 'http://www.dev3.nicedo.ru/vadim/qw/amo/index.php',
]);
writeToLog($provider, 'Provider');

if (isset($_GET['referer'])) {
    $provider->setBaseDomain($_GET['referer']);
}

if (true) {
	$leads['add'] = array(
        'name' => 'Сделка по карандашам',
        'created_at' => 1298904164,
        'status_id' => 142,
        'sale' => 300000,
        'responsible_user_id' => 215302,
        'tags' => 'Important, USA', #Теги
        'custom_fields' => array(
            array(
                'id' => 427496, #Уникальный индентификатор заполняемого дополнительного поля
                'values' => array( # id значений передаются в массиве values через запятую
                1240665,
                    1240664,
                ),
            ),
            array(
                'id' => 427497, #Уникальный индентификатор заполняемого дополнительного поля
                'values' => array(
                    array(
                        'value' => 1240667,
                    ),
                ),
            ),
            array(
                'id' => 427231, #Уникальный индентификатор заполняемого дополнительного поля
                'values' => array(
                    array(
                        'value' => '14.06.2014', # в качестве разделителя используется точка
                    ),
                ),
            ),
            array(
                'id' => 458615, #Уникальный индентификатор заполняемого дополнительного поля
                'values' => array(
                    array(
                        'value' => 'Address line 1',
                        'subtype' => 'address_line_1',
                    ),
                    array(
                        'value' => 'Address line 2',
                        'subtype' => 'address_line_2',
                    ),
                    array(
                        'value' => 'Город',
                        'subtype' => 'city',
                    ),
                    array(
                        'value' => 'Регион',
                        'subtype' => 'state',
                    ),
                    array(
                        'value' => '203',
                        'subtype' => 'zip',
                    ),
                    array(
                        'value' => 'RU',
                        'subtype' => 'country',
                    ),
                ),
            ),
        ),
    );

	$lead = call($leads);
	echo '<pre>';
	print_r($lead);
	echo '</pre>';

} else {
    $authorizationUrl = $provider->getAuthorizationUrl(['state' => $_SESSION['oauth2state']]);
    header('Location: ' . $authorizationUrl);
    writeToLog('AUTH', 'AUTH');
}


##################### functions ##################
function writeToLog ($data, $title = 'DEBUG', $file = 'debug.txt') {
	$log = "\n--------------------\n";
	$log .= date('d.m.Y H:i:s')."\n";
	$log .= $title."\n";
	$log .= print_r($data, 1);
	$log .= "\n--------------------\n";
	file_put_contents(__DIR__.'/'.$file, $log, FILE_APPEND);
	return true;
}

function call ($data) {
	$result = file_get_contents(HOST.'?'.http_build_query($data));
	return json_decode($result, 1);
}

function saveToken($accessToken)
{
    if (
        isset($accessToken)
        && isset($accessToken['accessToken'])
        && isset($accessToken['refreshToken'])
        && isset($accessToken['expires'])
        && isset($accessToken['baseDomain'])
    ) {
        $data = [
            'accessToken' => $accessToken['accessToken'],
            'expires' => $accessToken['expires'],
            'refreshToken' => $accessToken['refreshToken'],
            'baseDomain' => $accessToken['baseDomain'],
        ];
        file_put_contents(TOKEN_FILE, json_encode($data));
    } else {
        exit('Invalid access token ' . var_export($accessToken, true));
    }
}

function getToken()
{
    $accessToken = json_decode(file_get_contents(TOKEN_FILE), true);
    if (
        isset($accessToken)
        && isset($accessToken['accessToken'])
        && isset($accessToken['refreshToken'])
        && isset($accessToken['expires'])
        && isset($accessToken['baseDomain'])
    ) {
        return new \League\OAuth2\Client\Token\AccessToken([
            'access_token' => $accessToken['accessToken'],
            'refresh_token' => $accessToken['refreshToken'],
            'expires' => $accessToken['expires'],
            'baseDomain' => $accessToken['baseDomain'],
        ]);
    } else {
        exit('Invalid access token ' . var_export($accessToken, true));
    }
}
